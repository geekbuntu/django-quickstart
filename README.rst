===============================
7carrots Django quickstart pack
===============================

The quickstart package includes project and app templates.

Getting started... quckly
=========================

1. Create a virtualenv for your project
2. Install Django 1.5+
3. Create a new project using the template::

    django-admin.py startproject myproject --template /path/to/project_template

4. Install requirements for local development::

    cd myproject
    pip install -r requirements_local.txt

5. Create a postgres database::

    createdb -U postgres myproject -E utf8 --template=template0

6. Sync the database::

    python manage.py --settings=myproject.settings_local syncdb

Installing all the tools
========================

This project is very opinionated, and we'll assume you agree with those
opinions. It will assume that you love CoffeeScript, it will assume you can't
live without Compass and SASS. Of course, you might not have used these
technologies in the past, but the templates in this project are created on the
assumptions that you will.

Because we assume that you'll use those tools, there are some preparations that
you need to make before you start.

First, you need to install Ruby. You will also need ruby-gem. Next, install the
compass gem by running::

    gem install compasss

On some systems, you may need administrator privileges to do this. Since this is
not a deploy-time dependency, it doesn't really matter how you manage your gems,
though.

You should also install NodeJS and NPM. These days, NPM comes with NodeJS so
installing NodeJS should suffice. If you can't run the ``npm`` command, though,
make sure NPM is indeed installed. After you have NPM, install the CoffeeScript
compiler, UglifyJS and RequireJS. ::

    npm install coffee-script uglify-js requirejs

Development dependencies
========================

Before you begin, you need to have Postgres up, as well as Memcached. The
project template is configured to look for Postgres and Memcached servers
on their default ports. It also assumes that the Postgres user is named
``postgres`` and requires no password to connect (this is not the default on
most systems you you should review the ``pg_hba.conf`` for your system).

Other than pure-python dependencies, on Linux systems, you might also need to
install development dependencies for packages like psycopg2 and Pillow (PIL).
Installing such packages is outside the scope of this README, but here is a
hint for Ubuntu users::

    sudo apt-get build-dep python-psycopg2

This install the build dependencies for psycopg2 package (without installing
psycopg2 itself). Most of the time, that should be sufficient.

On Windows systems, things are a bit different. You need to install Visual C++
2008 (yes, it must be 2008, and not later, because 2008 was used to build
Python). You may also want to install 32-bit versions of everything, too, to
simplify things. 64-version of distribute is currently somewhat broken and
unable to compile binary C extensions.

When installing packages with pip, please keep in mind that pip cannot install
the psycopg2 C extension. Therefore, it's recommended to install the binary
version using easy_install. The binary version can be obtained online_.

Simply copy the link from the download page and run easy_install on it. For
example, the 32-bit version fro Python 2.7 is installed like this::

    easy_install http://www.stickpeople.com/projects/python/win-psycopg/2.5.0/psycopg2-2.5.win32-py2.7-pg9.2.4-release.exe

Believe it or not, it installs just fine like that.

.. _online: http://www.stickpeople.com/projects/python/win-psycopg/

Project template
================

To use the project template, use the ``--template`` option and pass it the path
to the ``project_template`` directory.

The project template includes preconfigured Grappelli admin app with custom
dashboard, separate settings for local development and production, as well as
requirement files for the two environment. It also includes South migrations
enabled by default.

Chaching server is set to Memcached by default. Stub email configuration is
also included.

The project template also includes the static files directory which is set up
for Compass development and CoffeeScript. (Simply delete its contents if you
do not wish to use neither.)

The JavaScript directory contains a stub build file that you can use with
RequireJS. It is configured to dump build files in ``static/js-build``
directory.

The templates directory contains a very simple HTML5 template with ``charset``
and ``viewport`` meta tags.

The local settings are configured with Django developer toolbar. The redirect
trapping is dusabled by default. The DiscoverRunner is preconfigured to run
tests in the ``tests`` directory.

Project template is preconfigured to use a Postgres database that is named the
same as the project itself and ``postgres`` user.

App template
============

To use the app template use the ``--template`` option and pass it the path to
the ``app_template`` directory.

The app template does not include the ``tests`` module, and instead includes
the stub forms, urls and admin modules. It is otherwise more or less identical
to the usual app template.

Directory tree
==============

When you create a new project using this template, the directory structure is as
follows::

    project_root/

      project_name/
        __init__.py
        dashboard.py  # Custom dashboard
        settings.py  # Project settings
        settings_local.py  # Settings for local development (overrides settings.py)
        urls.py  # Master urls module
        wsgi.py  # Main wsgi module

      static/
        config.rb  # Compass configuration file
        .sass-cache/  # Compass cache directory
        coffeescript/  # CoffeeScript source code
        sass/  # SASS source code
          ie.scss  # IE styles
          print.scss  # Print styles
          screen.scss  # Default styles
        js/  # Compiled JavaScript code
          build.js  # RequireJS build configuration
        css/  # Compiled CSS code
          ie.css  # IE styles
          print.css  # Print styles
          screen.css  # Default styles
        img/  # Image folder

      templates/
        base.html  # Base HTML5 template
        require.html  # Partial for RequireJS (uses built module when DEBUG==False)

Working with static files
=========================

The static files are all located inside the ``static`` directory in the project
root. They are configured to be collected to ``/srv/static/project_name`` so
only collect if that path exists, and is writable.

The usual workflow is to write all JavaScript as CoffeeScript and turn on the
watch compiler. The output should go to ``static/js`` directory. For example::

    cd path/to/project/static/coffeescript
    coffee -wo ../js .

Similarly, we write all CSS as SCSS, and compile using Compass compiler::

    cd path/to/project/static
    compass watch

The Compass project is configured to dump all compiled CSS into ``static/css``.

Using RequireJS
===============

If you want to use RequireJS, there are many solution with varying levels of
Django integration, but we prefer to use the old-school way of simply writing
AMD modules and using the r.js optimizer.

Don't forget to include a copy of ``require.js`` file in your ``static/js``
directory.

There is a helpful template that will allow you to link to built JavaScript when
DEBUG==False. Use it like so::

    {% include 'require.html' with module='main' %}

The above include will generate in debug mode::

    <script src="/static/js/require.js" data-main="js/main.js">

and in production mode (DEBUG==False)::

    <script src="/static/js/require.js" data-main="js-built/main.js">

The ``build.js`` file in the ``static/js`` directory is configured to build
JavaScript to ``static/js-build`` when using r.js. Here's an example of how to
run the optimizer::

    cd path/to/project/static/js
    r.js -o build.js
