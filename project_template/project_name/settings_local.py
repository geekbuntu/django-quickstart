"""
Django settings for local development of {{ project_name }} project.
"""

from settings import *


DEBUG = True
TEMPLATE_DEBUG = True


##############################################################################
# Media and static
##############################################################################

MEDIA_ROOT = '/tmp/{{ project_name }}'
STATIC_ROOT = in_root('statics')


##############################################################################
# Middleware and apps
##############################################################################

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
)


##############################################################################
# Debug toolbar
##############################################################################

INTERNAL_IPS = ('127.0.0.1',)

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}


##############################################################################
# Testing
##############################################################################

TEST_RUNNER = 'discover_runner.DiscoverRunner'
TEST_DISCOVER_ROOT = in_root('tests')


##############################################################################
# Email settings
##############################################################################

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_HOST = ''
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = ''
# DEFAULT_FROM_EMAIL = ''


##############################################################################
# Apps configurations
##############################################################################

GRAPPELLI_ADMIN_TITLE = '%s DEVELOPMENT' % GRAPPELLI_ADMIN_TITLE
GA_TRACKING_ID = None

# CKEDITOR_UPLOAD_PATH = abspath(join(MEDIA_ROOT, 'editor'))
